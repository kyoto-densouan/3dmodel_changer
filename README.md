# README #

1/3スケールの両替機風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK Fusion360です。

***

# 実機情報

## メーカ
- 不明

## 発売時期

- 不明

## 参考資料


***


![](https://bitbucket.org/kyoto-densouan/3dmodel_changer/raw/3e20b93faa048b98344fd20bf93bba44bae98fbb/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_changer/raw/3e20b93faa048b98344fd20bf93bba44bae98fbb/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_changer/raw/3e20b93faa048b98344fd20bf93bba44bae98fbb/ExampleImage.png)
